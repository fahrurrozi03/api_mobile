<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $guarded = ['id'];

    /*
     * Validations
     */

    public static function rules($update = false, $id = null)
    {
        $rules = [
            'name_store'    => 'required',
            'description'  => 'required'
        ];

        if ($update) {
            return $rules;
        }

        // return array_merge($rules, [
        //     'email'         => 'required|unique:colleagues,email',
        // ]);
    }
}
