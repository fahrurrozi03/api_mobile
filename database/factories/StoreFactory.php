<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Store::class, function (Faker $faker) {
    return [
        'name_store' => $faker->lastName,
        'urlToImage' => '',
        'description' => $faker->randomElement($array = array (
            'Older antibiotics like penicillin kill bacteria by preventing their cell wall from being built in the first place', 
            'Canadian scientists develop new antibiotic that prevents cell division - Down To Earth Magazine',
            'Arvind Kejriwal is also likely to retain all the cabinet ministers who were part of his previous government.')),
    ];
});
