<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

class Orders extends Model
{
    protected $guarded = ['id'];

    /*
     * Validations
     */

    public static function rules($update = false, $id = null)
    {
        $rules = [
            'order_name'    => 'required',
            'order_type'  => 'required'
        ];

        if ($update) {
            return $rules;
        }

        // return array_merge($rules, [
        //     'email'         => 'required|unique:colleagues,email',
        // ]);
    }
}