<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Orders::class, function (Faker $faker) {
    return [
        'order_name' => $faker->lastName,
        'order_type' => $faker->randomElement($array = array ('Makanan', 'Minuman')),
    ];
});
